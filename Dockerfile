FROM ubuntu:18.04

RUN apt-get update -y && apt-get install -y make
RUN apt-get update -y && apt-get install -y qemu-user
RUN apt-get update -y && apt-get install -y gcc
RUN apt-get update -y && apt-get install -y gcc-arm-linux-gnueabihf
RUN apt-get update -y && apt-get install -y gcc-aarch64-linux-gnu

WORKDIR /root

CMD /bin/bash