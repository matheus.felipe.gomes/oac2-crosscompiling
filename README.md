**Escola Politécnica da Universidade de São Paulo**

**Disciplina**: PCS3422 - Organizaçõa e Arquitetura de Computadores

**Nome**: Matheus Felipe Gomes

**NUSP**: 8993198

**Data**: 02 de Novembro de 2020

**GitLab**: [@matheus.felipe.gomes](https://gitlab.uspdigital.usp.br/matheus.felipe.gomes)

**Repositório GitLab**: [oac2-m9-crosscompiling](https://gitlab.uspdigital.usp.br/matheus.felipe.gomes/oac2-crosscompiling)

**Professor**: Dr. Bruno Albertini

**Título**: Módulo9 - Cross-Compilação de um benchmark

Escolheu-se a aplicação [automotive](http://vhosts.eecs.umich.edu/mibench/automotive.tar.gz) do [MiBench](http://vhosts.eecs.umich.edu/mibench/), apresentado na seção 2.1 "Automotive and Industrial Control" do paper [MiBench.pdf](https://gitlab.uspdigital.usp.br/matheus.felipe.gomes/oac2-crosscompiling/-/blob/master/MiBench.pdf). Primeiramente, apresentamos a seguir a estrutura do repositório [oac2-m9-crosscompiling](https://gitlab.uspdigital.usp.br/matheus.felipe.gomes/oac2-crosscompiling) que contém os arquivos utilizados e produzidos durante esta atividade. 


```
.
├── Dockerfile
├── Makefile
├── README.md
├── automotive
│   ├── basicmath
│   │   ├── COMPILE
│   │   ├── LICENSE
│   │   ├── Makefile
│   │   ├── basicmath_large.c
│   │   ├── basicmath_small.c
│   │   ├── bin
│   │   ├── cubic.c
│   │   ├── isqrt.c
│   │   ├── output
│   │   ├── pi.h
│   │   ├── rad2deg.c
│   │   ├── round.h
│   │   ├── runme_large.sh
│   │   ├── runme_small.sh
│   │   ├── snipmath.h
│   │   └── sniptype.h
│   ├── bitcount
│   │   ├── COMPILE
│   │   ├── LICENSE
│   │   ├── Makefile
│   │   ├── bin
│   │   ├── bitarray.c
│   │   ├── bitcnt_1.c
│   │   ├── bitcnt_2.c
│   │   ├── bitcnt_3.c
│   │   ├── bitcnt_4.c
│   │   ├── bitcnts.c
│   │   ├── bitfiles.c
│   │   ├── bitops.h
│   │   ├── bitstrng.c
│   │   ├── bstr_i.c
│   │   ├── conio.h
│   │   ├── extkword.h
│   │   ├── output
│   │   ├── runme_large.sh
│   │   ├── runme_small.sh
│   │   └── sniptype.h
│   ├── qsort
│   │   ├── COMPILE
│   │   ├── LICENSE
│   │   ├── Makefile
│   │   ├── bin
│   │   ├── input_large.dat
│   │   ├── input_small.dat
│   │   ├── output
│   │   ├── qsort_large.c
│   │   ├── qsort_small.c
│   │   ├── runme_large.sh
│   │   └── runme_small.sh
│   └── susan
│       ├── COMPILE
│       ├── LICENSE
│       ├── Makefile
│       ├── bin
│       ├── input_large.pgm
│       ├── input_small.pgm
│       ├── output
│       ├── runme_large.sh
│       ├── runme_small.sh
│       └── susan.c
└── figures

14 directories, 113 files
```

Utilizou-se uma imagem docker baseado na imagem `ubuntu:18.04` munido dos seguintes pacotes:
- make: utility for directing compilation
- gcc: GNU C compiler.
- qemu-user: QEMU user emulation
- gcc-arm-linux-gnueabihf: cross-compiler para arquitetura armhf. Utilizou-se o comando `arm-linux-gnueabihf-gcc`.
- gcc-aarch64-linux-gnu: cross-compiler para arquitetura aarch64 (ARMv8). Utilizou-se o comando `aarch64-linux-gnu-gcc`.

O arquivo `Dockerfile` abaixo é responsável por preparar tal imagem. Utilizamos o Makefile a seguir para construir e executar a imagem em um container. Utilizando o target `buildrun` do Makefile da raiz do repositório, a imagem docker é construída e um container é instanciado no modo interativo com volume docker ligado no diretório corrente `${PWD}`.

O aplicação `automotive` contém 4 benchmarks:
- basicmath: Execução de cálculos matemáticos simples que frequentemente não possuem hardware dedicado em processadores embarcados.
- bitcount: O algoritmo de contagem de bits testa as habilidades de manipulação de bits pelo processador efetuando a contagem do número de bits em um vetor de inteiros via 5 métodos distintos.
- qsort: Ordenação de grandes vetores de strings em ordem crescnete utilizando o conhecido algoritmo `quick-sort`.
- susan: Pacote de reconhecimento de imagem, desenvolvido para reconhecimento de cantos e bordas em imagens de resonância magnética do cérebro.

Para cada um dos benchmarks, o seguinte procedimento foi realizado:
- Modificação do Makefile para compilar os benchmarks com `gcc`, `arm-linux-gnueabihf-gcc`, e `aarch64-linux-gnu-gcc`
- Adição dos targets `run_small` e `run_large` no Makefile, substituindo o uso dos scripts sh `runme_small.sh` e `runme_large.sh`, respecticamente.
- Adição do target `all` para compilar (para x86, ARM, e ARMv8) e executar o benchmark para os conjuntos de entrada small e large.
- Execução do comando `make all`

## Benchmark automotive/basicmath

### Makefile

Acessar [Makefile](https://gitlab.uspdigital.usp.br/matheus.felipe.gomes/oac2-crosscompiling/-/blob/master/automotive/basicmath/Makefile).

```makefile
...

all: gcc arm aarch64 run_small run_large

gcc: basicmath_small_gcc basicmath_large_gcc

arm: basicmath_small_arm basicmath_large_arm

aarch64: basicmath_small_aarch64 basicmath_large_aarch64

basicmath_small_gcc: mkdirs ${FILE1} Makefile
	gcc -static -O3 ${FILE1} -o ${BIN}/basicmath_small -lm -Wno-all

basicmath_large_gcc: mkdirs ${FILE2} Makefile
	gcc -static -O3 ${FILE2} -o ${BIN}/basicmath_large -lm -Wno-all

basicmath_small_arm: mkdirs ${FILE1} Makefile
	arm-linux-gnueabihf-gcc -static -O3 ${FILE1} -o ${BIN}/basicmath_small_arm -lm -Wno-all

basicmath_large_arm: mkdirs ${FILE2} Makefile
	arm-linux-gnueabihf-gcc -static -O3 ${FILE2} -o ${BIN}/basicmath_large_arm -lm -Wno-all

basicmath_small_aarch64: mkdirs ${FILE1} Makefile
	aarch64-linux-gnu-gcc -static -O3 ${FILE1} -o ${BIN}/basicmath_small_aarch64 -lm -Wno-all

basicmath_large_aarch64: mkdirs ${FILE2} Makefile
	aarch64-linux-gnu-gcc -static -O3 ${FILE2} -o ${BIN}/basicmath_large_aarch64 -lm -Wno-all

...

run: run_small run_large

run_small:
	./${BIN}/basicmath_small > ${OUTPUT}/output_small.txt
	qemu-arm ${BIN}/basicmath_small_arm > ${OUTPUT}/output_small_arm.txt
	qemu-aarch64 ${BIN}/basicmath_small_aarch64 > ${OUTPUT}/output_small_aarch64.txt

run_large:
	./${BIN}/basicmath_large > ${OUTPUT}/output_large.txt
	qemu-arm ${BIN}/basicmath_large_arm > ${OUTPUT}/output_large_arm.txt
	qemu-aarch64 ${BIN}/basicmath_large_aarch64 > ${OUTPUT}/output_large_aarch64.txt
```

### Execução

![](figures/basicmath.png)

Os arquivos binários são armazenados no diretório `automotive/basicmath/bin` e os arquivos de saída no diretório `automotive/basicmath/output`:
```
bin
    ├── basicmath_large
    ├── basicmath_large_aarch64
    ├── basicmath_large_arm
    ├── basicmath_small
    ├── basicmath_small_aarch64
    └── basicmath_small_arm
output
    ├── output_large.txt
    ├── output_large_aarch64.txt
    ├── output_large_arm.txt
    ├── output_small.txt
    ├── output_small_aarch64.txt
    └── output_small_arm.txt
```

## Benchmark automotive/bitcount

### Makefile

Acessar [Makefile](https://gitlab.uspdigital.usp.br/matheus.felipe.gomes/oac2-crosscompiling/-/blob/master/automotive/bitcount/Makefile).

```makefile
...

all: gcc arm aarch64 run_small run_large

gcc: bitcnts_gcc

arm: bitcnts_arm

aarch64: bitcnts_aarch64

bitcnts_gcc: mkdirs ${FILE} Makefile
	gcc -static ${FILE} -O3 -o ${BIN}/bitcnts_gcc

bitcnts_arm: mkdirs ${FILE} Makefile
	arm-linux-gnueabihf-gcc -static ${FILE} -O3 -o ${BIN}/bitcnts_arm

bitcnts_aarch64: mkdirs ${FILE} Makefile
	aarch64-linux-gnu-gcc -static ${FILE} -O3 -o ${BIN}/bitcnts_aarch64

...

run_small:
	./${BIN}/bitcnts_gcc 75000 > ${OUTPUT}/output_small_gcc.txt
	qemu-arm ${BIN}/bitcnts_arm 75000 > ${OUTPUT}/output_small_arm.txt
	qemu-aarch64 ${BIN}/bitcnts_aarch64 75000 > ${OUTPUT}/output_small_aarch64.txt

run_large:
	./${BIN}/bitcnts_gcc 1125000 > ${OUTPUT}/output_large_gcc.txt
	qemu-arm ${BIN}/bitcnts_arm 1125000 > ${OUTPUT}/output_large_arm.txt
	qemu-aarch64 ${BIN}/bitcnts_aarch64 1125000 > ${OUTPUT}/output_large_aarch64.txt
```

### Execução

![](figures/bitcount.png)

Os arquivos binários são armazenados no diretório `automotive/bitcount/bin` e os arquivos de saída no diretório `automotive/bitcount/output`.
```
bin
    ├── bitcnts_aarch64
    ├── bitcnts_arm
    └── bitcnts_gcc
output
    ├── output_large_aarch64.txt
    ├── output_large_arm.txt
    ├── output_large_gcc.txt
    ├── output_small_aarch64.txt
    ├── output_small_arm.txt
    └── output_small_gcc.txt
```

## Benchmark automotive/qsort

### Makefile

Acessar [Makefile](https://gitlab.uspdigital.usp.br/matheus.felipe.gomes/oac2-crosscompiling/-/blob/master/automotive/qsort/Makefile).

```makefile
...

all: gcc arm aarch64 run_small run_large

...

qsort_small_gcc: mkdirs ${FILE1} Makefile
	gcc -static ${FILE1} -O3 -o ${BIN}/qsort_small_gcc -lm -Wno-format

qsort_large_gcc: mkdirs ${FILE2} Makefile
	gcc -static ${FILE2} -O3 -o ${BIN}/qsort_large_gcc -lm -Wno-format

qsort_small_arm: mkdirs ${FILE1} Makefile
	${CC_ARM} -static ${FILE1} -O3 -o ${BIN}/qsort_small_arm -lm -Wno-format

qsort_large_arm: mkdirs ${FILE2} Makefile
	${CC_ARM} -static ${FILE2} -O3 -o ${BIN}/qsort_large_arm -lm -Wno-format

qsort_small_aarch64: mkdirs ${FILE1} Makefile
	${CC_AARCH64} -static ${FILE1} -O3 -o ${BIN}/qsort_small_aarch64 -lm -Wno-format

qsort_large_aarch64: mkdirs ${FILE2} Makefile
	${CC_AARCH64} -static ${FILE2} -O3 -o ${BIN}/qsort_large_aarch64 -lm -Wno-format

...

run_small:
	${BIN}/qsort_small_gcc input_small.dat > ${OUTPUT}/output_small_gcc.txt
	qemu-arm ${BIN}/qsort_small_arm input_small.dat > ${OUTPUT}/output_small_arm.txt
	qemu-aarch64 ${BIN}/qsort_small_aarch64 input_small.dat > ${OUTPUT}/output_small_aarch64.txt

run_large:
	${BIN}/qsort_large_gcc input_large.dat > ${OUTPUT}/output_large_gcc.txt
	qemu-arm ${BIN}/qsort_large_arm input_large.dat > ${OUTPUT}/output_large_arm.txt
	qemu-aarch64 ${BIN}/qsort_large_aarch64 input_large.dat > ${OUTPUT}/output_large_aarch64.txt
```

### Execução

![](figures/qsort.png)

Os arquivos binários são armazenados no diretório `automotive/qsort/bin` e os arquivos de saída no diretório `automotive/qsort/output`.
```
bin
    ├── qsort_large_aarch64
    ├── qsort_large_arm
    ├── qsort_large_gcc
    ├── qsort_small_aarch64
    ├── qsort_small_arm
    └── qsort_small_gcc
output
    ├── output_large_aarch64.txt
    ├── output_large_arm.txt
    ├── output_large_gcc.txt
    ├── output_small_aarch64.txt
    ├── output_small_arm.txt
    └── output_small_gcc.txt
```


## Benchmark automotive/susan

### Makefile

Acessar [Makefile](https://gitlab.uspdigital.usp.br/matheus.felipe.gomes/oac2-crosscompiling/-/blob/master/automotive/susan/Makefile).

```makefile
...

all: gcc arm aarch64

...

susan_gcc: mkdirs ${FILE} Makefile
	gcc -static -O4 -o ${BIN}/susan_gcc ${FILE} -lm -Wno-all -Wno-unused-result

susan_arm: mkdirs ${FILE} Makefile
	${CC_ARM} -static -O4 -o ${BIN}/susan_arm ${FILE} -lm -Wno-all -Wno-unused-result

susan_aarch64: mkdirs ${FILE} Makefile
	${CC_AARCH64} -static -O4 -o ${BIN}/susan_aarch64 ${FILE} -lm -Wno-all -Wno-unused-result

...

run_small_aarch64:
	qemu-aarch64 ${BIN}/susan_aarch64 input_small.pgm ${OUTPUT}/output_small_aarch64.smoothing.pgm -s
	qemu-aarch64 ${BIN}/susan_aarch64 input_small.pgm ${OUTPUT}/output_small_aarch64.edges.pgm -e
	qemu-aarch64 ${BIN}/susan_aarch64 input_small.pgm ${OUTPUT}/output_small_aarch64.corners.pgm -c

run_large_aarch64:
	qemu-aarch64 ${BIN}/susan_aarch64 input_large.pgm ${OUTPUT}/output_large_aarch64.smoothing.pgm -s
	qemu-aarch64 ${BIN}/susan_aarch64 input_large.pgm ${OUTPUT}/output_large_aarch64.edges.pgm -e
	qemu-aarch64 ${BIN}/susan_aarch64 input_large.pgm ${OUTPUT}/output_large_aarch64.corners.pgm -c
```

### Execução

![](figures/susan.png)

Os arquivos binários são armazenados no diretório `automotive/susan/bin` e os arquivos de saída no diretório `automotive/susan/output`.
```
bin
    ├── susan
    ├── susan_aarch64
    ├── susan_arm
    └── susan_gcc
output
    ├── output_large_aarch64.corners.pgm
    ├── output_large_aarch64.edges.pgm
    ├── output_large_aarch64.smoothing.pgm
    ├── output_large_arm.corners.pgm
    ├── output_large_arm.edges.pgm
    ├── output_large_arm.smoothing.pgm
    ├── output_large_gcc.corners.pgm
    ├── output_large_gcc.edges.pgm
    ├── output_large_gcc.smoothing.pgm
    ├── output_small_aarch64.corners.pgm
    ├── output_small_aarch64.edges.pgm
    ├── output_small_aarch64.smoothing.pgm
    ├── output_small_arm.corners.pgm
    ├── output_small_arm.edges.pgm
    ├── output_small_arm.smoothing.pgm
    ├── output_small_gcc.corners.pgm
    ├── output_small_gcc.edges.pgm
    └── output_small_gcc.smoothing.pgm
```

## Conclusão

Para todos os benchmarks, realizou-se a compilação para x86 e a cross-compilação para ARM e ARMv8 (aarch64). A execução dos mesmos utilizando `qemu-user` foi realizada em seguida. Verificou-se finalmente a igualdade dos arquivos de saída para cada benchmark.

# Referências

- [MiBench](http://vhosts.eecs.umich.edu/mibench/)
- [Transparently running binaries from any architecture in Linux with QEMU and binfmt_misc](https://ownyourbits.com/2018/06/13/transparently-running-binaries-from-any-architecture-in-linux-with-qemu-and-binfmt_misc/)
- [QEMU](https://www.qemu.org/)
- [gcc-aarch64-linux-gnu](https://packages.ubuntu.com/xenial/gcc-aarch64-linux-gnu)