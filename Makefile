build:
	docker build -t oac2_m9_ubuntu .

run:
	docker run -it -v ${PWD}:/root/oac2-m9-crosscompiling oac2_m9_ubuntu

buildrun: build run